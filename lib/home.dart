import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  List<dynamic> products = [];

  //get data from api call
  void getData() async {
    var url = "http://172.21.242.70:3000/products";
    var res = await http.get(Uri.parse(url));

    if (res.statusCode == 200) {
      var jsonData = jsonDecode(res.body);
      setState(() {
        products = jsonData;
      });
    } else {
      print(res);
    }
  }

  //update data using value and id
  void updateData(id, stock) async {
    var url = 'http://172.21.242.70:3000/products/' + id;
    var res = await http.put(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "Prod_Stock": stock,
      }),
    );

    if (res.statusCode == 200) {
      getData();
    } else {
      print(res);
    }
  }

  //delete using id
  void deleteData(id) async {
    var url = 'http://172.21.242.70:3000/products/' + id;
    var res = await http.delete(Uri.parse(url));

    if (res.statusCode == 200) {
      getData();
    } else {
      print(res);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              child: products.isEmpty
                  ? TextButton(
                      onPressed: () {
                        getData();
                      },
                      child: Text(
                        'Get Data',
                        style: TextStyle(fontSize: 30),
                        textAlign: TextAlign.center,
                      ),
                    )
                  : Text(
                      'Product Data',
                      style: TextStyle(fontSize: 30),
                      textAlign: TextAlign.center,
                    ),
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: products.length,
                itemBuilder: (context, index) {
                  return Row(
                    children: [
                      Icon(
                        Icons.water_drop_outlined,
                        color: Colors.greenAccent,
                        size: 50,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10, top: 20),
                        child: Column(
                          children: [
                            Text(
                              products[index]['Prod_Title'],
                              style: TextStyle(fontSize: 25),
                            ),
                            Text(
                              products[index]['Prod_Quantity'] + '/ml',
                              style: TextStyle(
                                  fontSize: 20, color: Colors.black38),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 70),
                        child: GestureDetector(
                          child: Text(
                            products[index]['Prod_Stock'],
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color:
                                    products[index]['Prod_Stock'] == 'In Stock'
                                        ? Colors.green
                                        : Colors.redAccent),
                          ),
                          onTap: () {
                            if (products[index]['Prod_Stock'] == 'In Stock') {
                              updateData(products[index]['id'], 'Out Stock');
                            } else {
                              updateData(products[index]['id'], 'In Stock');
                            }
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30),
                        child: GestureDetector(
                          child: Icon(
                            Icons.delete_forever_outlined,
                            size: 35,
                            color: Colors.redAccent,
                          ),
                          onTap: () {
                            deleteData(products[index]['id']);
                          },
                        ),
                      )
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }
}
